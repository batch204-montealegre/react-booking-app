import { useEffect, useState } from 'react';
// import courseData from '../data/courseData';
import CourseCard from '../components/CourseCard';




export default function Courses(){
	// console.log(courseData)
	// console.log(courseData[0])

	const [coursesData, setCoursesData] = useState([])

	useEffect(() => {
		console.log(process.env.REACT_APP_API_URL)

		fetch(`${process.env.REACT_APP_API_URL}/courses`)
		.then(res => res.json())
		.then(data => {
			console.log(data)
			setCoursesData(data)
		})
	}, [])


	const courses = coursesData.map(course => {
		return(
			<CourseCard courseProp={course} key={course._id}/>
		)
	})

	return (
		<>
			<h1>Courses</h1>
			{courses}
		</>
	)
}

