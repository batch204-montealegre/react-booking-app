import {useState,useEffect} from 'react';
import {Row, Col, Card, Button} from 'react-bootstrap'

export default function CourseCard({courseProps}){
    // console.log(courseProps)

    const {name, description, price} = courseProps

    const [count, setCount] = (useState(0));
    console.log(useState(0));
    const [seats, setSeats] = (useState(30));

    function enroll () {
        if (seats>0){
        setCount(count + 1)
        setSeats(seats-1)
    }

    else{
        alert("No more seats")
    }
}

useEffect(() => {
    console.log("State changed")
}, [count, seats])

	return(
		        <Row className="my-3">
            <Col>
                <Card className="cardHighlight p-3">
                    <Card.Body>
                        <Card.Title>
                            <h2>{name}</h2>
                        </Card.Title>
                        <Card.Subtitle className = "mt-3">Description: </Card.Subtitle>
                        <Card.Text>
                        	
                            {description}
                        </Card.Text>

                        <Card.Subtitle className = "mt-3">Price: </Card.Subtitle>
                        <Card.Text className = "mb-3">
                        	{price}
                        </Card.Text>
                        <Card.Subtitle>Enrollees:</Card.Subtitle>
                         <Card.Text>{count} Enrollees</Card.Text>



                        <Button variant="primary" onClick={enroll}>Enroll</Button>

                           <Card.Subtitle className = "mt-3">Seats Available:</Card.Subtitle>
                         <Card.Text>{seats} Seats</Card.Text>

                    </Card.Body>
                </Card>
                </Col>
        </Row>
		)
}