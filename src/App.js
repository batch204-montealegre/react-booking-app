import { useState } from 'react';
import {Container} from 'react-bootstrap';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom'
import './App.css';
import AppNavBar from './components/AppNavBar';
import Courses from './pages/Courses'
import Home from './pages/Home'
import Register from './pages/Register'
import Login from './pages/Login'
import Logout from './pages/Logout'
import Error from './pages/Error'
import { UserProvider } from "./UserContext"


function App() {

  const [user, setUser] = useState({email:localStorage.getItem("email")})

  return (
    <UserProvider value={{user, setUser}}>
      <Router>
        <>
          <AppNavBar />
          <Container>
            <Switch>
              <Route exact path="/" component={Home}/>
              <Route exact path="/courses" component={Courses}/>
              <Route exact path="/register" component={Register}/>
              <Route exact path="/login" component={Login}/>
              <Route exact path="/logout" component={Logout}/>
              <Route component={Error}/>
            </Switch>
          </Container>
        </>
      </Router>
    </UserProvider>
  );
}


export default App;
